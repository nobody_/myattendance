#!/usr/bin/python2
import sys, pycurl, getopt
from BeautifulSoup import BeautifulSoup
from StringIO import StringIO
from getpass import getpass
from collections import OrderedDict

myattendance_url = ''
domain           = ''
gate_url         = ''
socks5_host      = ''
socks5_port      = 0
conf = OrderedDict([
  ('user1', 'pass1'),
  ('user2', 'pass2')
])

def status(attendance_table):
  today  = attendance_table.find('tr', attrs={'class':' myAttendanceTodayRow'}) # there should be a space in the beginning of class name
  w_time = today.findAll('td')[1].text.split(':')
  n_time = today.findAll('td')[4].text.split(':')

  w_time_t = int(w_time[0]) * 60 + int(w_time[1])
  n_time_t = int(n_time[0]) * 60 + int(n_time[1])

  l_time_t = n_time_t - w_time_t
  if( l_time_t < 0 ):
    return 'Run, you fool'
  l_time = '%02u:%02u' % (l_time_t / 60, l_time_t % 60)
  return l_time

def myattendance(attendance_table):
  columns = attendance_table.findAll('th') 
  rows    = attendance_table.findAll('tr')
  total   = attendance_table.find('tr', attrs={'class':'MyAttendanceSubtotalRow'})
  w_total = total.findAll('span')[0]
  f_total = total.findAll('span')[2]
  n_total = total.findAll('td')[4]
  ret     = ''

  del columns[0:6] # Day, Working Time, Frame Time, Approved Time, Norm Time, Approval Type
  del rows[0]      # First row
  del rows[-2:]    # Sub total, Total
  del total        # clean up

  ret += ('Day\t\tWork\tFrame\t')
  for col in columns:
    col = col.text
    if col == 'OUT':
      ret += ('   OUT    ')
    elif col == 'IN':
      ret += ('   IN   ')
    else:
      sys.stderr.write('Something is wrong with columns\n')
      sys.exit(1)
  ret += '\n'

  for row in rows:
    days   = row.find('td')
    times  = row.findAll('td', attrs={'class':{'inTimeCell','outTimeCell'}})
    w_time = row.findAll('span')[0]
    f_time = row.findAll('span')[2]

    out = ' '.join(map(lambda x: x.text if len(x.text) else ' '*(len('00:00:00')), times))
    out = '\t'.join([days.text, w_time.text, f_time.text, out])
    ret += (out + '\n')
  ret += '\n'
  ret += ('Total:\t\t' + w_total.text + '\t' + f_total.text + '\n')
  ret += ('Need:\t\t'  + n_total.text + '\n')
  return ret

def usage():
  sys.stderr.write('Usage: %s [OPTION]...\n\n' % sys.argv[0] + \
    '--username=username\tActive Directory account name\n' + \
    '--status\t\tShow only left working time for today\n'
  )
  sys.exit(1)

def main():
  username = password = ''
  action = 0
  try:
    optlist, args = getopt.getopt(sys.argv[1:], ':', ['status', 'username=', 'help'])
  except getopt.GetoptError as e:
    sys.stderr.write(str(e) + '\n')
    sys.stderr.write('Try %s --help for more information.\n' % sys.argv[0])
    sys.exit(1)

  for arg, value in optlist:
    if arg == '--username':
      username = value
    elif arg == '--status':
      action = 1
    elif arg == '--help':
      usage()

  if not username:
    username, password = conf.items()[0]
  elif conf.get(username):
    password = conf.get(username)
  elif not conf.get(username):
    password = getpass('Password: ')

  attendance_html   = StringIO()
  attendance_header = StringIO()
  ch = pycurl.Curl()
  opts = [
    (ch.URL,             myattendance_url),
    (ch.VERBOSE,         False),
    (ch.USERAGENT,       ''),
    (ch.HTTPAUTH,        ch.HTTPAUTH_NTLM),
    (ch.USERPWD,         domain + '\\' + username + ':' + password),
    (ch.WRITEFUNCTION,   attendance_html.write),
    (ch.HEADERFUNCTION,  attendance_header.write),
    (ch.CONNECTTIMEOUT,  10),
    (ch.TIMEOUT,         10)
  ]
  for k, v in opts:
    ch.setopt(k, v)

  try:
    ch.perform()
    ch.close()
  except:
    sys.stderr.write('Error while communication with ' + myattendance_url + '\n')
    sys.exit(1)

  attendance_html = BeautifulSoup(attendance_html.getvalue(), convertEntities=BeautifulSoup.HTML_ENTITIES)
  try:
    attendance_table =  attendance_html.body.find('table', attrs={'class':'myAttendanceMasterView'})
  except:
    sys.stderr.write('> ' + attendance_header.getvalue().split('\r\n')[0] + '\n')
    sys.exit(1)

  ret = ''
  ## action
  if action == 0:
    ret = myattendance(attendance_table)
  elif action == 1:
    ret = status(attendance_table)
  else:
    sys.stderr.write('What?\n')
  sys.stdout.write(ret)
  
  if not gate_url:
    sys.exit(0)

  ch = pycurl.Curl()
  opts = [
    (ch.URL,            gate_url),
    (ch.PROXYTYPE,      ch.PROXYTYPE_SOCKS5_HOSTNAME),
    (ch.PROXY,          socks5_host),
    (ch.PROXYPORT,      socks5_port),
    (ch.VERBOSE,        False),
    (ch.WRITEFUNCTION,  lambda x: None),
    (ch.POST,           True),
    (ch.POSTFIELDS,     ret.encode('utf-8')),
    (ch.CONNECTTIMEOUT, 10),
    (ch.TIMEOUT,        10)
  ]
  for k, v in opts:
    ch.setopt(k,v)

  try:
    ch.perform()
    ch.close()
  except:
    sys.exit(2)

if __name__ == '__main__':
  main()
